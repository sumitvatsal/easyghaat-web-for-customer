﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyGhaat.Models
{
    public class ViewModel
    {

        public IEnumerable<EasyGhaat.Models.home_slider> sliders { get; set; } 
        public IEnumerable<EasyGhaat.Models.Welocme_area> welcome { get; set; }
        public IEnumerable<EasyGhaat.Models.testimonial> testimonials { get; set; }
        public IEnumerable<EasyGhaat.Models.about_stripe> about_stripes { get; set; }
        public IEnumerable<EasyGhaat.Models.service_page_stripe> service_page_stripes { get; set; }
        public IEnumerable<EasyGhaat.Models.Aditional_servicepage> Aditional_servicepages { get; set; }
        public IEnumerable<EasyGhaat.Models.wlcm_srvic_stripe> wlcm_srvic_stripes { get; set; }
        public IEnumerable<EasyGhaat.Models.Footer_content> Footer_content { get; set; }
        public IEnumerable<EasyGhaat.Models.UpperFooter> UpperFooters { get; set; }
        public IEnumerable<EasyGhaat.Models.contact_content> contact_content { get; set; }
        public IEnumerable<EasyGhaat.Models.Offer> Offer { get; set; }
        public IEnumerable<EasyGhaat.Models.State> State { get; set; }
        public IEnumerable<EasyGhaat.Models.City> City { get; set; }
        public IEnumerable<EasyGhaat.Models.Branch> Branch { get; set; }
        public IEnumerable<EasyGhaat.Models.RoleType> RoleType { get; set; }
        public IEnumerable<EasyGhaat.Models.Category> Category { get; set; }
        public IEnumerable<EasyGhaat.Models.Service> Service { get; set; }
        public IEnumerable<EasyGhaat.Models.Gender> Gender { get; set; }
        public IEnumerable<EasyGhaat.Models.Employee> Employee { get; set; }
        public IEnumerable<EasyGhaat.Models.Proc_GetOrders_Result> Proc_GetOrders_Result { get; set; }
        public IEnumerable<EasyGhaat.Models.Product> Product { get; set; }
        public IEnumerable<EasyGhaat.Models.PriceTable> PriceTable { get; set; }
        public IEnumerable<EasyGhaat.Models.Vendor> Vendor { get; set; }
        public IEnumerable<EasyGhaat.Models.Unit_Type> Unit_Type { get; set; }
        public IEnumerable<EasyGhaat.Models.AssignService_Vendor> AssignService_Vendor { get; set; }
        public IEnumerable<EasyGhaat.Models.DeliveryType_tbl> DeliveryType_tbl { get; set; }
        public IEnumerable<EasyGhaat.Models.Order_tbl> Order_tbl { get; set; }
        public IEnumerable<EasyGhaat.Models.OrderCancel> OrderCancel { get; set; }
        public IEnumerable<EasyGhaat.Models.ReferRoleType> ReferRoleTypes { get; set; }
        public IEnumerable<EasyGhaat.Models.Applying_pro> Applying_pros { get; set; }
        public IEnumerable<EasyGhaat.Models.Locality> Locality { get; set; }
        public IEnumerable<EasyGhaat.Models.Promo_Type> Promo_Type { get; set; }
    }
}