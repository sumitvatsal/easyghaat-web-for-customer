﻿using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin
{
    public class MasterAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MasterAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MasterAdmin_default",
                "MasterAdmin/{controller}/{action}/{id}",
                new { Controller="Home",  action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}