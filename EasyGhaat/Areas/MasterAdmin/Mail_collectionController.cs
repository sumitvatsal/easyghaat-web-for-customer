﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Areas.MasterAdmin
{
    public class Mail_collectionController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/Mail_collection
        public IQueryable<Mail_collection> GetMail_collection()
        {

            return db.Mail_collection;
        }

        // GET: api/Mail_collection/5
        [ResponseType(typeof(Mail_collection))]
        public IHttpActionResult GetMail_collection(int id)
        {
            Mail_collection mail_collection = db.Mail_collection.Find(id);
            if (mail_collection == null)
            {
                return NotFound();
            }

            return Ok(mail_collection);
        }

        // PUT: api/Mail_collection/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMail_collection(int id, Mail_collection mail_collection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mail_collection.id)
            {
                return BadRequest();
            }

            db.Entry(mail_collection).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Mail_collectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Mail_collection
        [ResponseType(typeof(Mail_collection))]
        public IHttpActionResult PostMail_collection(Mail_collection mail_collection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Mail_collection.Add(mail_collection);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mail_collection.id }, mail_collection);
        }

        // DELETE: api/Mail_collection/5
        [ResponseType(typeof(Mail_collection))]
        public IHttpActionResult DeleteMail_collection(int id)
        {
            Mail_collection mail_collection = db.Mail_collection.Find(id);
            if (mail_collection == null)
            {
                return NotFound();
            }

            db.Mail_collection.Remove(mail_collection);
            db.SaveChanges();

            return Ok(mail_collection);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Mail_collectionExists(int id)
        {
            return db.Mail_collection.Count(e => e.id == id) > 0;
        }
    }
}