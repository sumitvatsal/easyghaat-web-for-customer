﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class EmployeeRejactionReasonController : Controller
    {

        AdminContext db = new AdminContext();
        // GET: MasterAdmin/EmployeeRejactionReason
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");
        }



        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            EmployeeRejactionReason objEmployeeRejactionReason = new EmployeeRejactionReason();
            try
            {
                objEmployeeRejactionReason.Reason = frm["Reason"];
                objEmployeeRejactionReason.IsActive = true;
                objEmployeeRejactionReason.IsDeleted = false;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                objEmployeeRejactionReason.CreateDateTime = indianTime;
                db.EmployeeRejactionReasons.Add(objEmployeeRejactionReason);
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                throw ex;

            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

    }
}