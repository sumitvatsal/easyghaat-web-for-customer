﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewBranchDetailController : Controller
    {
        // GET: MasterAdmin/ViewBranchDetail
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
     
                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");
        }
    }
}