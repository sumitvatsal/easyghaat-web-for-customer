﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddContactUsController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AddContactUs
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.contact_content.ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");


        }


        [HttpPost]
        public ActionResult Submit()
        {
            try
            { 
                    contact_content obj = new contact_content();                  
                    obj.address = Request.Form["Address"];
                    obj.contact_no = Request.Form["ContactNo"];
                    obj.email = Request.Form["Email"];
                    db.contact_content.Add(obj);
                    db.SaveChanges();
                    
                TempData["Message"] = "Successfull";
                return RedirectToAction("Index");
              
            }
            catch(Exception e)
            {

            }


            return View();

        }


        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.contact_content.Where(c => c.id.Equals(id)).SingleOrDefault();
            db.contact_content.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        public ActionResult Edit(int id)
        {
            return View(db.contact_content.Where(c => c.id.Equals(id)).SingleOrDefault());
        }

        //Edit Testimonials
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            contact_content obj = new contact_content();
            obj.id = Convert.ToInt32(frm["hdnId"]);
            obj = db.contact_content.Find(obj.id);
            if (obj != null)
            {
                obj.id = Convert.ToInt32(frm["hdnId"]);
                obj.address = frm["Address"];
                obj.contact_no = frm["ContactNo"];
                obj.email = frm["Email"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("../AddContactUs/Index");
        }






    }
}