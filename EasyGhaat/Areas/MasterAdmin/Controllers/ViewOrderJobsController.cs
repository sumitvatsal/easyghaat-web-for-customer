﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewOrderJobsController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ViewOrderJobs
        public ActionResult Index( string id)
        {
            int oid = Convert.ToInt32(id);
            Session["OrderId"] = oid;
            //var obj = db.Database.SqlQuery<Proc_GetOrderJobs_Result>("Proc_GetOrderJobs  @OrderId",
            //    new SqlParameter("@OrderId", oid)
            //    ).ToList<Proc_GetOrderJobs_Result>();
            return View();

          
        }



        //[HttpGet]
        //public JsonResult GetOrderJobs( string sidx, string sord, int page=0, int rows=0, string OrderId=null)
        //{
        //    using (AdminContext db = new AdminContext())
        //    {
        //        int pageIndex = Convert.ToInt32(page) - 1;
        //       // Session["BranchId"] = ddlBranch;
        //        int pageSize = rows;
        //        var obj = db.Database.SqlQuery<Proc_GetOrderJobs_Result>("Proc_GetOrderJobs  @OrderId",
        //        new SqlParameter("@OrderId", OrderId)
        //        ).ToList<Proc_GetOrderJobs_Result>();

        //        int totalRecords = obj.Count();
        //        var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
        //        if (sord.ToUpper() == "DESC")
        //        {
        //            obj = obj.OrderByDescending(s => s.EmailId).ToList();
        //            obj = obj.Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //        }
        //        else
        //        {
        //            obj = obj.OrderBy(s => s.EmailId).ToList();
        //            obj = obj.Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //        }
        //        var jsonData = new
        //        {
        //            total = totalPages,
        //            page,
        //            records = totalRecords,
        //            rows = obj
        //        };
        //        return Json(jsonData, JsonRequestBehavior.AllowGet);
        //    }
        //}



    }
}