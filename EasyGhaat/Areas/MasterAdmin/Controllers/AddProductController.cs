﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddProductController : Controller
    {

        AdminContext dbAdminContext = new AdminContext();

        // GET: MasterAdmin/AddProduct
        public ActionResult Index()
        {       
            int SessionId = Convert.ToInt32(Session["AdminId"]);
            if (SessionId != 0)
            {
                ViewModel vm = new ViewModel();
                vm.Category = dbAdminContext.Categories.Where(c => c.IsActive == true && c.IsDeleted == false).ToList();
                vm.Gender = dbAdminContext.Genders.ToList();
                return View(vm);
            }
            return RedirectToAction("../../AdminLogin/Index");

        }





        [HttpPost]
        public ActionResult Add(HttpPostedFileBase[] files, FormCollection frm)
        {
            Product obj = new Product();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images/" + pic;
                    obj.ImgUrl = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    obj.Name = frm["Name"];
                    obj.Description = frm["Description"];
                    obj.CategoryId = Convert.ToInt32(frm["ddlCategory"]);
                    if (obj.CategoryId == 4)
                    {
                        obj.GenderId = 0;
                    }

                    else
                    {

                        obj.GenderId = Convert.ToInt32(frm["ddlgender"]);
                        if (obj.GenderId == 0)
                        {
                            TempData["NotSpecified"] = "Successfull";
                            return RedirectToAction("Index");
                        }
                    }
        
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    obj.CreateDate = indianTime;
                    dbAdminContext.Products.Add(obj);
                    dbAdminContext.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}