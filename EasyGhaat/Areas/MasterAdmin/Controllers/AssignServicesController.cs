﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AssignServicesController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AssignServices
        public ActionResult Index()
        {



            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {


                Session["Vid"] = Session["VendorId"];
                int id = Convert.ToInt32(Session["Vid"]);
                var Result = db.Vendors.Where(v => v.Id == id).SingleOrDefault();
                var ResultBranch = db.Branches.Where(b => b.Id == Result.AssignBranchId).SingleOrDefault();
                if (ResultBranch != null)
                {
                    TempData["BranchName"] = ResultBranch.Name;
                }
                if (Result != null)
                {
                    TempData["VendorName"] = Result.VendorName;
                }
                return View(db.AssignService_Vendor.Where(a => a.VendorId == id).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");






        }



        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            AssignService_Vendor obj = new AssignService_Vendor();

            int VendorId = Convert.ToInt32(Session["Vid"]);
            int first = Convert.ToInt32(frm["chkWash&Iron"]);
            int second = Convert.ToInt32(frm["chkSteamIron"]);
            int third = Convert.ToInt32(frm["chkPremiumWash"]);
            int fourth = Convert.ToInt32(frm["chkDryClean"]);
            int fivth = Convert.ToInt32(frm["chkOnlyIron"]);
            if(first!=0)
            {
                var chkf = db.AssignService_Vendor.Where(f => f.ServiceId == first && f.VendorId== VendorId).SingleOrDefault();
                if(chkf!=null)
                    {

                   // obj.Id = chkf.Id;
                    obj.ServiceId = Convert.ToInt32(frm["chkWash&Iron"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Entry(chkf).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                }
                else
                { 

                obj.ServiceId = Convert.ToInt32(frm["chkWash&Iron"]);
                obj.VendorId = Convert.ToInt32(Session["Vid"]);
                obj.IsActive = true;
                obj.IsDeleted = false;
                obj.CreateDate = DateTime.UtcNow;
                db.AssignService_Vendor.Add(obj);
                db.SaveChanges();

                }
            }
            if (second != 0)
            {
                var chks = db.AssignService_Vendor.Where(f => f.ServiceId == second && f.VendorId == VendorId).SingleOrDefault();

                if(chks!=null)
                {
                   // obj.Id = chks.Id;
                    obj.ServiceId = Convert.ToInt32(frm["chkSteamIron"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Entry(chks).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                }
                else
                {

                    obj.ServiceId = Convert.ToInt32(frm["chkSteamIron"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.AssignService_Vendor.Add(obj);
                    db.SaveChanges();

                }

            }
            if (third != 0)
            {

                var chkt = db.AssignService_Vendor.Where(f => f.ServiceId == third && f.VendorId == VendorId).SingleOrDefault();
                if (chkt != null)
                {
                   // obj.Id = chkt.Id;
                    obj.ServiceId = Convert.ToInt32(frm["chkPremiumWash"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Entry(chkt).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    obj.ServiceId = Convert.ToInt32(frm["chkPremiumWash"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.AssignService_Vendor.Add(obj);
                    db.SaveChanges();
                }
            }


                if (fourth != 0)
                {
                    var chkft = db.AssignService_Vendor.Where(f => f.ServiceId == fourth && f.VendorId == VendorId).SingleOrDefault();
                    if (chkft != null)
                    {

                   // obj.Id = chkft.Id;
                    obj.ServiceId = Convert.ToInt32(frm["chkDryClean"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Entry(chkft).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                }

                else
                    {
                        obj.ServiceId = Convert.ToInt32(frm["chkDryClean"]);
                        obj.VendorId = Convert.ToInt32(Session["Vid"]);
                        obj.IsActive = true;
                        obj.IsDeleted = false;
                        obj.CreateDate = DateTime.UtcNow;
                        db.AssignService_Vendor.Add(obj);
                        db.SaveChanges();

                    }
                }


            if (fivth != 0)
            {

                var fth = db.AssignService_Vendor.Where(f => f.ServiceId == fivth && f.VendorId == VendorId).SingleOrDefault();
                if (fth != null)
                {
                   // obj.Id = fth.Id;
                    obj.ServiceId = Convert.ToInt32(frm["chkDryClean"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Entry(fth).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    obj.ServiceId = Convert.ToInt32(frm["chkOnlyIron"]);
                    obj.VendorId = Convert.ToInt32(Session["Vid"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.AssignService_Vendor.Add(obj);
                    db.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("../ViewVendor/Index");
            //int id= Convert.ToInt32(Session["Vid"]);
            //return RedirectToAction("../AddVendorPriceList/Index", new { id = id });
            //here we can use this AddVendorPrice url with paramterevId......
        }



    }
}