﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;



namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddDeliveryType_SurgePriceController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AddDeliveryType_SurgePrice
        public ActionResult Index()
        {


            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                ViewModel mymodel = new ViewModel();
                mymodel.Branch = db.Branches.Where(b=>b.IsDeleted==false).ToList();
                mymodel.DeliveryType_tbl = db.DeliveryType_tbl.ToList();
                return View(mymodel);
            }

            return RedirectToAction("../../AdminLogin/Index");
        }
        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            DeliveryType_SurgePrice obj = new DeliveryType_SurgePrice();          
            var BranchId= int.Parse(frm["ddlBranch"]);
            var DeliveryTypeId = int.Parse(frm["ddlType"]);
            var objCheck = db.DeliveryType_SurgePrice.Where(d => d.BranchId == BranchId && d.DeliveryTypeId == DeliveryTypeId  && d.IsDeleted==false).SingleOrDefault();

            if (objCheck != null)
            {

                TempData["Message-1"] = "Successfull";
                return RedirectToAction("Index");

            }

            else
            {
                obj.BranchId = int.Parse(frm["ddlBranch"]);
                obj.DeliveryTypeId = int.Parse(frm["ddlType"]);
                obj.SurgePercentage = int.Parse(frm["txtSurgePrice"]);
                obj.IsDeleted = false;
                obj.IsActive = true;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                obj.CreateDate = indianTime;
                db.DeliveryType_SurgePrice.Add(obj);
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

    }
}