﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddLaundryBagController : Controller
    {

        private AdminContext db = new AdminContext();

        // GET: MasterAdmin/AddLaundryBag
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");
        }


        [HttpPost]
        public ActionResult Add(HttpPostedFileBase[] files, FormCollection frm)
        {
            LaundryBag_Tbl obj = new LaundryBag_Tbl();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images" + pic;
                    obj.BagImgUrl = relativePath;
                    file.SaveAs(Server.MapPath(relativePath));
                    file.SaveAs(Server.MapPath(relativePath));
                    obj.MaterialName = frm["MaterialName"];
                    obj.CountCapacity = Convert.ToInt32(frm["Count"]);
                    obj.weightCapacity = Convert.ToInt32(frm["weight"]);
                    obj.Price = Convert.ToInt32(frm["Price"]);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    obj.CreateDate = indianTime;
                    obj.ModifiedDate = indianTime;
                    db.LaundryBag_Tbl.Add(obj);
                    db.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }







    }
}