﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ManageEmployeeController : Controller
    {
        AdminContext db = new AdminContext();
        ViewModel mymodel = new ViewModel();
        Employee objEmp = new Employee();
        SalaryDetails objSalaryDetail = new SalaryDetails();
        EmployeeAddress objEmpAddress = new EmployeeAddress();
        EmployeeBankAccount_Tbl ObjEmployeeBankAccount_Tbl = new EmployeeBankAccount_Tbl();
        public int Valchk;
        public int _min = 1000;
        public int _max = 9999;


        // GET: MasterAdmin/ManageEmployee
        public ActionResult Index()
        {
            try
            {
                int SessionId = Convert.ToInt32(Session["AdminId"]);
                if (SessionId != 0)
                {
                    mymodel.City = db.Cities.ToList();
                    mymodel.Branch = db.Branches.ToList();
                    mymodel.RoleType = db.RoleTypes.Where(r => r.RoleName != "Customer").ToList();
                    return View(mymodel);
                }
                return RedirectToAction("../../AdminLogin/Index");
            }
            catch (Exception ex)

            {
                throw ex;
            }
        }




        [HttpPost]
        public ActionResult Add(FormCollection frm, HttpPostedFileBase[] files)
        {
            try
            {
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        string relativePath = @"/Content/images" + pic;
                        objEmp.uplod_photo = relativePath;
                        file.SaveAs(Server.MapPath(relativePath));
                        objEmp.emp_name = frm["Name"];
                        objEmp.password = frm["Password"];
                        objEmp.per_phoneNo = frm["PPhoneNo"];
                        objEmp.EmailId= frm["EmailId"];
                        objEmp.IsActive = true;
                        objEmp.Isdeleted = false;
                        objEmp.assin_roleId = int.Parse(frm["ddlARole"]);
                        objEmp.asin_branchId = int.Parse(frm["ddlABranch"]);
                        objEmp.DOJ =  Convert.ToDateTime(frm["DOJ"]);
                        objEmp.DOB = Convert.ToDateTime(frm["DOB"]);
                        TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                        DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                        objEmp.CreatedDate = indianTime;
                        Random _rdm = new Random();
                        string m = _rdm.Next(_min, _max).ToString();
                        objEmp.SecretPin = Convert.ToInt32(m);
                        db.Employees.Add(objEmp);
                        db.SaveChanges();
                        //Getting Current EmployeeId From databse Employee Table
                        int EmployeeId = objEmp.emp_id;
                        //Fetchingf Record From db to check Employee Record Exists or not
                        var ResultEmpRecord = db.Employees.Where(e => e.emp_id == EmployeeId && e.Isdeleted == false && e.IsActive == true).SingleOrDefault();
                        if (ResultEmpRecord != null)
                        {
                            Valchk = Convert.ToInt32(frm["hdnchk"]);
                            if (Valchk == 1)
                            {
                                //Current Address
                                objEmpAddress.EmployeeId = ResultEmpRecord.emp_id;
                                objEmpAddress.AddTypeId = 4;
                                objEmpAddress.CityId = Convert.ToInt32(frm["ddlCityCurrent"]);
                                objEmpAddress.AreaId = frm["AreaIdCurrent"];
                                objEmpAddress.HouseNo = frm["HouseNoCurrent"];
                                objEmpAddress.StreetName = frm["StreetNameCurrent"];
                                objEmpAddress.LandMark = frm["LandMarkCurrent"];
                                objEmpAddress.Longitude = frm["LongitudeCurrent"];
                                objEmpAddress.Lattitude = frm["LattitudeCurrent"];
                                objEmpAddress.IsActive = true;
                                objEmpAddress.IsDeleted = false;
                                objEmpAddress.CreateDate = indianTime;
                                db.EmployeeAddresses.Add(objEmpAddress);
                                db.SaveChanges();
                                //Premanent Address
                                objEmpAddress.EmployeeId = ResultEmpRecord.emp_id;
                                objEmpAddress.AddTypeId = 5;
                                objEmpAddress.CityId = Convert.ToInt32(frm["ddlCityCurrent"]);
                                objEmpAddress.AreaId = frm["AreaIdCurrent"];
                                objEmpAddress.HouseNo = frm["HouseNoCurrent"];
                                objEmpAddress.StreetName = frm["StreetNameCurrent"];
                                objEmpAddress.LandMark = frm["LandMarkCurrent"];
                                objEmpAddress.Longitude = frm["LongitudeCurrent"];
                                objEmpAddress.Lattitude = frm["LattitudeCurrent"];
                                objEmpAddress.IsActive = true;
                                objEmpAddress.IsDeleted = false;
                                objEmpAddress.CreateDate = indianTime;
                                db.EmployeeAddresses.Add(objEmpAddress);
                                db.SaveChanges();
                            }

                            if (Valchk == 2)
                            {
                                //Current Address
                                objEmpAddress.EmployeeId = ResultEmpRecord.emp_id;
                                objEmpAddress.AddTypeId = 4;
                                objEmpAddress.CityId = Convert.ToInt32(frm["ddlCityCurrent"]);
                                objEmpAddress.AreaId = frm["AreaIdCurrent"];
                                objEmpAddress.HouseNo = frm["HouseNoCurrent"];
                                objEmpAddress.StreetName = frm["StreetNameCurrent"];
                                objEmpAddress.LandMark = frm["LandMarkCurrent"];
                                objEmpAddress.Longitude = frm["LongitudeCurrent"];
                                objEmpAddress.Lattitude = frm["LattitudeCurrent"];
                                objEmpAddress.IsActive = true;
                                objEmpAddress.IsDeleted = false;
                                objEmpAddress.CreateDate = indianTime;
                                db.EmployeeAddresses.Add(objEmpAddress);
                                db.SaveChanges();
                                //Premanent Address
                                objEmpAddress.EmployeeId = ResultEmpRecord.emp_id;
                                objEmpAddress.AddTypeId = 5;
                                objEmpAddress.CityId = Convert.ToInt32(frm["ddlCityPermanent"]);
                                objEmpAddress.AreaId = frm["AreaIdPermanent"];
                                objEmpAddress.HouseNo = frm["HouseNoPermanent"];
                                objEmpAddress.StreetName = frm["StreetNamePermanent"];
                                objEmpAddress.LandMark = frm["LandMarkPermanent"];
                                objEmpAddress.Longitude = frm["LongitudePermanent"];
                                objEmpAddress.Lattitude = frm["LattitudePermanent"];
                                objEmpAddress.IsActive = true;
                                objEmpAddress.IsDeleted = false;
                                objEmpAddress.CreateDate = indianTime;
                                db.EmployeeAddresses.Add(objEmpAddress);
                                db.SaveChanges();
                            }
                            //Insert into Salary Detail
                            objSalaryDetail.EmpId = ResultEmpRecord.emp_id;
                            objSalaryDetail.BasicSalary = Convert.ToDouble(frm["BasicSalary"]);
                            objSalaryDetail.TA = Convert.ToDouble(frm["TA"]);
                            objSalaryDetail.DA = Convert.ToDouble(frm["DA"]);
                            objSalaryDetail.HRA = Convert.ToDouble(frm["HRA"]);
                            objSalaryDetail.IsActive = true;
                            objSalaryDetail.IsDeleted = false;
                            objSalaryDetail.CreateDate = indianTime;
                            db.SalaryDetails.Add(objSalaryDetail);
                            db.SaveChanges();
                            //Indert into Employee Bank Detail
                            ObjEmployeeBankAccount_Tbl.EmployeeId = ResultEmpRecord.emp_id;
                            ObjEmployeeBankAccount_Tbl.BankName = frm["BankName"];
                            ObjEmployeeBankAccount_Tbl.IFSC = frm["IFSCCode"];
                            ObjEmployeeBankAccount_Tbl.BranchName = frm["BranchName"];
                            ObjEmployeeBankAccount_Tbl.MICR = frm["MICR"];
                            ObjEmployeeBankAccount_Tbl.BankAccountNumber = frm["BankAccountNo"];
                            int AcTypeId = Convert.ToInt32(frm["ddlAccountType"]);
                            if (AcTypeId == 1)
                            {
                                ObjEmployeeBankAccount_Tbl.AccountType = "Saving";

                            }
                            if (AcTypeId == 2)
                            {
                                ObjEmployeeBankAccount_Tbl.AccountType = "Current";

                            }
                            ObjEmployeeBankAccount_Tbl.IsActive = true;
                            ObjEmployeeBankAccount_Tbl.IsDeleted = false;
                            ObjEmployeeBankAccount_Tbl.CreateDate = indianTime;
                            db.EmployeeBankAccount_Tbl.Add(ObjEmployeeBankAccount_Tbl);
                            db.SaveChanges();
                      
                        }
                    }
                }
                TempData["Message"] = "successfull";
                return RedirectToAction("Index");
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }


        [HttpGet]
        public JsonResult CheckPhoneNo(Employee objEmployee)
        {
            var Result = db.Employees.Where(pc => pc.per_phoneNo == objEmployee.per_phoneNo && pc.IsActive == true && pc.Isdeleted == false).SingleOrDefault();
            if (Result != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult CheckEmailId(Employee objEmployee)
        {
            var Result = db.Employees.Where(pc => pc.EmailId == objEmployee.EmailId && pc.IsActive == true && pc.Isdeleted == false).SingleOrDefault();
            if (Result != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }















    }
}