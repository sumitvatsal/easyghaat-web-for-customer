﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewVendorPriceListController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ViewVendorPriceList
        public ActionResult Index(int id)
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {


                return View(db.VendorPrice_tbl.Where(vp => vp.VendorId == id && vp.IsDeleted == false).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");

           
        }


        // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.VendorPrice_tbl.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            VendorPrice_tbl obj = new VendorPrice_tbl();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.VendorPrice_tbl.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);
                obj.Price = Convert.ToDouble(frm["txtPrice"]);
               // obj.ModifiedDate = DateTime.UtcNow;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
   
            return RedirectToAction("../UpdateVendor/Index");
        }


        public ActionResult Delete(int id)
        {
            var obj = db.VendorPrice_tbl.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
     
            return RedirectToAction("../UpdateVendor/Index");
        }


    }
}