﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewPromoCodesController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ViewPromoCodes
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {




                var Result = db.Database.SqlQuery<SP_ViewPromoCode_Result>("SP_ViewPromoCode").ToList<SP_ViewPromoCode_Result>();
                return View(Result);
            }

            return RedirectToAction("../../AdminLogin/Index");


        }
    }
}