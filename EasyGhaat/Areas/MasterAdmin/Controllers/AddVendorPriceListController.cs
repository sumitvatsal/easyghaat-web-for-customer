﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;



namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddVendorPriceListController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AddVendorPriceList


       public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);
            if (SessionId != 0)
            {             
                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");

        }

       public ActionResult Search(FormCollection frm)
        { 
            List<int> vendorIdList = new List<int>();
            int id= Convert.ToInt32(frm["ddlVendor"]);
            Session["VendorId"] = id;
            var li = db.Database.SqlQuery<Proc_GetPriceId_Result>("Proc_GetPriceId  @VendorId",
             new SqlParameter("@VendorId", id)
             ).ToList<Proc_GetPriceId_Result>();
            for (int i = 0; i <= li.Count - 1; i++)
            {
                vendorIdList.Add(li[i].id);
            }
            int[] a = vendorIdList.ToArray();
            var result = db.PriceTables.Where(b => a.Contains(b.Id) && b.IsDeleted == false);
            return View(result);
   
        }


        [HttpPost]
        public void getValues2(VendorPrice_tbl newPrice)
        {
            int vid = Convert.ToInt32(Session["VendorId"]);
            var res = db.VendorPrice_tbl.Where(vp => vp.VendorId == vid && vp.PriceId == newPrice.PriceId && vp.IsDeleted == false).SingleOrDefault();
            if (res != null)
            {
                return;
            }
            newPrice.VendorId = Convert.ToInt32(Session["VendorId"]);
            newPrice.IsDeleted = false;
            db.VendorPrice_tbl.Add(newPrice);
            db.SaveChanges();
            RedirectToAction("ViewVendorPrice/Index");          
        }


        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.VendorPrice_tbl.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Search");
        }



    }
}