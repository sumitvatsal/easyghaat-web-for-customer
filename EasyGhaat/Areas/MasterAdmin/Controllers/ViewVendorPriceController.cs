﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewVendorPriceController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ViewVendorPrice
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View();

            }

            return RedirectToAction("../../AdminLogin/Index");
        }


        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.VendorPrice_tbl.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Search");
        }


      public ActionResult Search(FormCollection frm)
        {
            int id = Convert.ToInt32(frm["ddlVendor"]);
            Session["VendorId"] = id;
            var result = db.Database.SqlQuery<Proc_ViewVendorPrice_Result >("Proc_ViewVendorPrice  @VendorId",
             new SqlParameter("@VendorId", id)
             ).ToList<Proc_ViewVendorPrice_Result>();
            return View(result);
        }





    }
}