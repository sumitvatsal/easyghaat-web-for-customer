﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ManageReferralController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ManageReferral
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {


                ViewModel vm = new ViewModel();
                vm.Branch = db.Branches.ToList();
                return View(vm);
            }

            return RedirectToAction("../../AdminLogin/Index");

        }



        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            try
            {
            ManageReferral obj = new ManageReferral();
            obj.BranchId = int.Parse(frm["ddlBranch"]);
            obj.ReferralAmount = frm["ReferralAmount"];
            obj.ValidityDays = frm["Validity"];
            obj.IsActive = true;
            obj.IsDeleted = false;
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            obj.CreateDate = indianTime;
            db.ManageReferrals.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

        }








    }
}