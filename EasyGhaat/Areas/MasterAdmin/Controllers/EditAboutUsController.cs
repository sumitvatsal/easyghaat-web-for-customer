﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class EditAboutUsController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/EditAboutUs
     public ActionResult Index()
      {


            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {





                return View(db.about_stripe.SingleOrDefault());
            }

            return RedirectToAction("../../AdminLogin/Index");







        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save(FormCollection fm)
        {
            about_stripe obj = new about_stripe();

            obj.id = Convert.ToInt32(fm["hdnId"]);
            obj = db.about_stripe.Find(obj.id);
            if (obj != null)
            {
                obj.id = Convert.ToInt32(fm["hdnId"]);
                obj.title = fm["title"];
                obj.sub_title = fm["subtitle"];
                obj.about_content = fm["aboutcontent"];
                obj.ReadMore_Content = fm["ReadMorecontent"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }
    }
}