﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddSurgeController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/AddSurge
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                ViewModel mymodel = new ViewModel();
                mymodel.Branch = db.Branches.ToList();
                mymodel.Locality = db.Locality.ToList();
                return View(mymodel);
            }

            return RedirectToAction("../../AdminLogin/Index");

        }


        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            Surge obj = new Surge();
            obj.BranchId = int.Parse(frm["ddlBranch"]);
            obj.AreaId = int.Parse(frm["ddlArea"]);
            obj.SurgeAmount = frm["SurgeAmount"];      
            obj.IsActive = true;
            obj.Isdelete = false;
            obj.Createdate = DateTime.UtcNow;
            db.Surges.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");

        }

    }
}