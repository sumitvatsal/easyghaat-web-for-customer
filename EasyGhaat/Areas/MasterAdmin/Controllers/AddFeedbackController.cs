﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddFeedbackController : Controller
    {


        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AddFeedback
        public ActionResult Index()
        {


            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
              return View(db.CustomerFeedbacks.ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");

        }




        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.CustomerFeedbacks.Where(c => c.Id.Equals(id)).SingleOrDefault();
            db.CustomerFeedbacks.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}