﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewProductsController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ViewProducts
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.Products.Where(p => p.IsActive == true && p.IsDeleted == false).Include(p=>p.Category).Include(p=>p.Gender).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");
          
        }

        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.Products.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                obj.IsActive = false;

                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



    }
}