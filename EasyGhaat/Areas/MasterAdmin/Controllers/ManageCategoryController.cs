﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ManageCategoryController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ManageCategory
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {





                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");

        }



        [HttpPost]
        public ActionResult Add( FormCollection frm)
        {
                    Category obj = new Category();
                    obj.Name = frm["Name"];
                    obj.Description = frm["Description"];
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Categories.Add(obj);
                    db.SaveChanges();
                
            
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }
    }
}