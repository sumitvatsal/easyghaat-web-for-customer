﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Runtime.Serialization;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateBranchController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpdateBranch
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.Branches.Include(ab=>ab.City).Where(ab=>ab.IsDeleted==false).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");
    
        }

   

        public ActionResult DoActive(string id)
        {                
                int k = int.Parse(id);
                Branch brnchobj = db.Branches.Find(k);           
                if (brnchobj.IsActive == false)
                {
                    brnchobj.IsActive = true;
                    db.Entry(brnchobj).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);                                     
        }


       public ActionResult DoUnActive(string id)
        {
            int k = int.Parse(id);
            Branch brnchobj = db.Branches.Find(k);
            if (brnchobj.IsActive == true)
            {
                brnchobj.IsActive = false;
                db.Entry(brnchobj).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Json(true, JsonRequestBehavior.AllowGet);

        }






       // For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.Branches.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if(obj!=null)
            {
                obj.IsActive = false;
                obj.IsDeleted = true;
                obj.CreatedDate = DateTime.UtcNow;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.Branches.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            Branch obj = new Branch();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.Branches.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);
                obj.Name = frm["BranchName"];
                obj.Address = frm["BranchAddress"];
                obj.PhoneNumber = frm["PhoneNumber"];
                obj.FaxNo = frm["FaxNumber"];
                obj.EmailId = frm["EmailId"];
                obj.IsActive = false;
                obj.IsDeleted = false;
                obj.CreatedDate = DateTime.UtcNow;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }













    }
}
