﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddReferrralAmountController : Controller
    {

        AdminContext db = new AdminContext();

        //GET:MasterAdmin/AddReferrralAmount
        public ActionResult Index()
        {



            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                ViewModel mymodel = new ViewModel();
                mymodel.ReferRoleTypes = db.ReferRoleTypes.Where(r => r.IsActive == true && r.IsDeleted == false).ToList();
                return View(mymodel);
            }

            return RedirectToAction("../../AdminLogin/Index");




        }

        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            try
            {
                int RoleID   = Convert.ToInt32(frm["ddlReferRole"]);
                int amnt= Convert.ToInt32(frm["txtAmount"]);

                var Result = db.ReferalAmount_Tbl.Where(r => r.Refer_Roleid == RoleID && r.IsDeleted == false).SingleOrDefault();

                if(Result!=null)
                {

                    TempData["Message-1"] = "Exists";
                    return RedirectToAction("Index");
                }

                else
                { 
                ReferalAmount_Tbl obj = new ReferalAmount_Tbl();
                obj.Refer_Roleid = Convert.ToInt32(frm["ddlReferRole"]);
                obj.Amount = Convert.ToInt32(frm["txtAmount"]);
                obj.IsActive = true;
                obj.IsDeleted = false;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                obj.CreateDate = indianTime;
                db.ReferalAmount_Tbl.Add(obj);
                db.SaveChanges();
                TempData["Message"] = "msz";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return RedirectToAction("Index");
        }


    }




}