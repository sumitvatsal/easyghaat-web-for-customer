﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class EditPriceListController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/EditPriceList
        public ActionResult Index(int id)
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);
            if (SessionId != 0)
            {

                return View(db.PriceTables.Where(c => c.Id.Equals(id)).SingleOrDefault());
            }
            return RedirectToAction("../../AdminLogin/Index");
        }




        [HttpPost]
        public ActionResult Edit( FormCollection frm)
        {
            PriceTable obj = new PriceTable();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.PriceTables.Find(obj.Id);
                 if (obj != null)
                       {            
                        obj.Id = Convert.ToInt32(frm["hdnId"]);
                        obj.IsActive = false;
                        db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                      }
            obj.Price = Convert.ToDouble(frm["Price"]);
            obj.ServiceId = obj.ServiceId;
            obj.BranchId = obj.BranchId;
            obj.ProductId = obj.ProductId;
            obj.UnitId = obj.UnitId;
            obj.IsActive = true;
            obj.IsDeleted = false;
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
            obj.CreateDate = indianTime;
            db.PriceTables.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("../ViewPriceList/Index");
        }

    }

 }




    
