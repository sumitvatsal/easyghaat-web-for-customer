﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewReferralAmountController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ViewReferralAmount
        public ActionResult Index()
        {


            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {


                return View(db.ReferalAmount_Tbl.Where(r => r.IsActive == true && r.IsDeleted == false).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");


           
        }




        //For Delete
        public ActionResult Delete(int id)
        {

            try
            {
                var obj = db.ReferalAmount_Tbl.Where(c => c.Id.Equals(id)).SingleOrDefault();
                if (obj != null)
                {
                    obj.Id = id;
                    obj.IsDeleted = true;
                    obj.IsActive = false;
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    obj.ModifiedDate = indianTime;
                    db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
     
            }

            catch (Exception ex)
            {

                throw ex;

            }

            return RedirectToAction("Index");

        }
    }
}