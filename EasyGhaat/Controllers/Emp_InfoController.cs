﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class Emp_InfoController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/Emp_Info
        public IQueryable<Emp_Info> GetEmp_Infos()
        {
            return db.Emp_Infos;
        }     
        [Route("api/Emp_Info/GetAdminLogin/{email}/{pwd}")]
        public IQueryable<Emp_Info> GetAdminLogin(string email, string pwd)
        {
            return db.Emp_Infos.Where(e => e.EmailId == email && e.password == pwd && e.IsActive == true && e.Isdeleted == false && e.RoleName == "Admin");
        }

        // GET: api/Emp_Info/5
        [ResponseType(typeof(Emp_Info))]
        public async Task<IHttpActionResult> GetEmp_Info(int id)
        {
            Emp_Info emp_Info = await db.Emp_Infos.FindAsync(id);
            if (emp_Info == null)
            {
                return NotFound();
            }

            return Ok(emp_Info);
        }

        // PUT: api/Emp_Info/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEmp_Info(int id, Emp_Info emp_Info)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != emp_Info.emp_id)
            {
                return BadRequest();
            }

            db.Entry(emp_Info).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Emp_InfoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Emp_Info
        [ResponseType(typeof(Emp_Info))]
        public async Task<IHttpActionResult> PostEmp_Info(Emp_Info emp_Info)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Emp_Infos.Add(emp_Info);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = emp_Info.emp_id }, emp_Info);
        }

        // DELETE: api/Emp_Info/5
        [ResponseType(typeof(Emp_Info))]
        public async Task<IHttpActionResult> DeleteEmp_Info(int id)
        {
            Emp_Info emp_Info = await db.Emp_Infos.FindAsync(id);
            if (emp_Info == null)
            {
                return NotFound();
            }

            db.Emp_Infos.Remove(emp_Info);
            await db.SaveChangesAsync();

            return Ok(emp_Info);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Emp_InfoExists(int id)
        {
            return db.Emp_Infos.Count(e => e.emp_id == id) > 0;
        }
    }
}