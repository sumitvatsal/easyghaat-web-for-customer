﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;
using System.Data.SqlClient;


namespace EasyGhaat.Controllers
{
    public class GetEmpAddressController : ApiController
    {
        private AdminContext db = new AdminContext();

        [HttpGet]
        [ActionName("ViewEmpAddress")]
        [Route("api/GetEmpAddress/ViewEmpAddress/{EmployeeId}")]
        public async Task<IHttpActionResult> ViewEmpAddress(int EmployeeId)
        {     
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Connection.connstring);
            try
            {
                DateTime ToDate = DateTime.Now;
                DateTime FromDate = ToDate.AddDays(-1);
                await con.OpenAsync();
                SqlCommand cmd = new SqlCommand("Proc_GetEmpAddress", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    dt.Load(reader);
                }
                if ((dt == null) || (dt.Rows.Count == 0))
                {
                    return NotFound();
                }
                con.Close();
                return Ok(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
