﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;


namespace EasyGhaat.Controllers
{
    public class AdminLoginController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: AdminLogin
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PostEmp_Session(EmployeeSession Emp_Session)
        {
            try
            {

                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                Emp_Session.Login_DateTime = indianTime;
                db.EmployeeSessions.Add(Emp_Session);
                db.SaveChanges();
                var res = db.EmployeeSessions.Where(e => e.EmployeeId == Emp_Session.EmployeeId).OrderBy(i => i.Id).ToList();
                if (res != null)
                {
                    Session["SessionId"] = Emp_Session.Id;
                    Session["AdminId"] = Emp_Session.EmployeeId;
                }
                return Json(true, JsonRequestBehavior.AllowGet);
                //  return RedirectToAction("../MasterAdmin/ViewProducts/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}