﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class ViewGetCancelReasonsController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/ViewGetCancelReasons
        [Route("api/ViewGetCancelReasons/{RoleId}")]
        public IQueryable<ViewGetCancelReason> GetViewGetCancelReasons(int RoleId)
        {
            return db.ViewGetCancelReasons.Where(cr => cr.RoleId == RoleId && cr.IsActive == true && cr.IsDeleted == false);
        }

        // GET: api/ViewGetCancelReasons/5
        [ResponseType(typeof(ViewGetCancelReason))]
        public async Task<IHttpActionResult> GetViewGetCancelReason(int id)
        {
            ViewGetCancelReason viewGetCancelReason = await db.ViewGetCancelReasons.FindAsync(id);
            if (viewGetCancelReason == null)
            {
                return NotFound();
            }

            return Ok(viewGetCancelReason);
        }

        // PUT: api/ViewGetCancelReasons/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutViewGetCancelReason(int id, ViewGetCancelReason viewGetCancelReason)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viewGetCancelReason.Id)
            {
                return BadRequest();
            }

            db.Entry(viewGetCancelReason).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViewGetCancelReasonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ViewGetCancelReasons
        [ResponseType(typeof(ViewGetCancelReason))]
        public async Task<IHttpActionResult> PostViewGetCancelReason(ViewGetCancelReason viewGetCancelReason)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ViewGetCancelReasons.Add(viewGetCancelReason);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = viewGetCancelReason.Id }, viewGetCancelReason);
        }

        // DELETE: api/ViewGetCancelReasons/5
        [ResponseType(typeof(ViewGetCancelReason))]
        public async Task<IHttpActionResult> DeleteViewGetCancelReason(int id)
        {
            ViewGetCancelReason viewGetCancelReason = await db.ViewGetCancelReasons.FindAsync(id);
            if (viewGetCancelReason == null)
            {
                return NotFound();
            }

            db.ViewGetCancelReasons.Remove(viewGetCancelReason);
            await db.SaveChangesAsync();

            return Ok(viewGetCancelReason);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViewGetCancelReasonExists(int id)
        {
            return db.ViewGetCancelReasons.Count(e => e.Id == id) > 0;
        }
    }
}