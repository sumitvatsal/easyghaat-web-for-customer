﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class ViewGetVendorAddressesController : ApiController
    {
        private AdminContext db = new AdminContext();

        [Route("api/ViewGetVendorAddresses/{VendorId}")]
        public IQueryable<ViewGetVendorAddress> GetViewGetVendorAddresses(int VendorId)
        {
            return db.ViewGetVendorAddresses.Where(v => v.VendorId == VendorId && v.IsDeleted==false );
        }

        // GET: api/ViewGetVendorAddresses/5
        [ResponseType(typeof(ViewGetVendorAddress))]
        public async Task<IHttpActionResult> GetViewGetVendorAddress(int id)
        {
            ViewGetVendorAddress viewGetVendorAddress = await db.ViewGetVendorAddresses.FindAsync(id);
            if (viewGetVendorAddress == null)
            {
                return NotFound();
            }

            return Ok(viewGetVendorAddress);
        }

        // PUT: api/ViewGetVendorAddresses/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutViewGetVendorAddress(int id, ViewGetVendorAddress viewGetVendorAddress)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viewGetVendorAddress.Id)
            {
                return BadRequest();
            }

            db.Entry(viewGetVendorAddress).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViewGetVendorAddressExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ViewGetVendorAddresses
        [ResponseType(typeof(ViewGetVendorAddress))]
        public async Task<IHttpActionResult> PostViewGetVendorAddress(ViewGetVendorAddress viewGetVendorAddress)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ViewGetVendorAddresses.Add(viewGetVendorAddress);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = viewGetVendorAddress.Id }, viewGetVendorAddress);
        }

        // DELETE: api/ViewGetVendorAddresses/5
        [ResponseType(typeof(ViewGetVendorAddress))]
        public async Task<IHttpActionResult> DeleteViewGetVendorAddress(int id)
        {
            ViewGetVendorAddress viewGetVendorAddress = await db.ViewGetVendorAddresses.FindAsync(id);
            if (viewGetVendorAddress == null)
            {
                return NotFound();
            }

            db.ViewGetVendorAddresses.Remove(viewGetVendorAddress);
            await db.SaveChangesAsync();

            return Ok(viewGetVendorAddress);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViewGetVendorAddressExists(int id)
        {
            return db.ViewGetVendorAddresses.Count(e => e.Id == id) > 0;
        }
    }
}