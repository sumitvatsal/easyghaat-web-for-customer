﻿using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace EasyGhaat.Controllers.FrontEndCMS
{
    public class OurLocationController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: OurLocation
        public ActionResult Index()
        {
           // db.add_location.SingleOrDefault();
            return View(db.add_location.SingleOrDefault());
        }
    }
}
