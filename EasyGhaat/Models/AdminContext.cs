﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace EasyGhaat.Models
{
    public class AdminContext : DbContext
    {


        public AdminContext()
           : base("name=AdminContext")
        {
        }
        public DbSet<State> States { get; set; }
        public DbSet<GetRole> GetRoles { get; set; }
        public DbSet<GetBranch> GetBranchs { get; set; }
        public DbSet<GetEmployeebyRoleAndBranch> GetEmployeebyRoleAndBranchs { get; set; }
        public DbSet<GetEmployeeDetail> GetEmployeeDetails { get; set; }
        public DbSet<GetEmployeeAddress> GetEmployeeAddresses { get; set; }
        public DbSet<Emp_Info> Emp_Infos { get; set; }
        public DbSet<ViewGetAssignedServiceByVendorId> ViewGetAssignedServiceByVendorIds { get; set; }
        public DbSet<ViewGetVendorByBranch> ViewGetVendorByBranchs { get; set; }
        public DbSet<ViewGetVendorAddress> ViewGetVendorAddresses { get; set; }
        public DbSet<ViewGetVendorDetail> ViewGetVendorDetails { get; set; }
        public DbSet<ViewGetVendorByBranchId> ViewGetVendorByBranchIds { get; set; }
        public DbSet<ViewGetCancelReason> ViewGetCancelReasons { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<PromoCode_Service> PromoCode_Service { get; set; }
        public DbSet<Applying_pro> Applying_pro { get; set; }
        public DbSet<EmployeeSession> EmployeeSessions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public virtual DbSet<about_stripe> about_stripe { get; set; }
        public virtual DbSet<ActivityNote> ActivityNotes { get; set; }
        public virtual DbSet<add_location> add_location { get; set; }
        public virtual DbSet<Add_logo> Add_logo { get; set; }
        public virtual DbSet<Add_SignUp> Add_SignUp { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Address_type> Address_type { get; set; }
        public virtual DbSet<Aditional_servicepage> Aditional_servicepage { get; set; }
        public virtual DbSet<AssignService_Vendor> AssignService_Vendor { get; set; }
        public virtual DbSet<Attendance> Attendances { get; set; }
        public virtual DbSet<BankAccount_Tbl> BankAccount_Tbl { get; set; }
        public virtual DbSet<BankCashSubmit_Tbl> BankCashSubmit_Tbl { get; set; }
        public virtual DbSet<BankTransfer_Tbl> BankTransfer_Tbl { get; set; }
        public virtual DbSet<BankTransferReason_Tbl> BankTransferReason_Tbl { get; set; }
        public virtual DbSet<BeneficiaryType_Tbl> BeneficiaryType_Tbl { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<BranchOffer> BranchOffers { get; set; }
        public virtual DbSet<CancelReason> CancelReasons { get; set; }
        public virtual DbSet<Carpet_caltbl> Carpet_caltbl { get; set; }
        public virtual DbSet<Cashback_tbl> Cashback_tbl { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<contact_content> contact_content { get; set; }
        public virtual DbSet<DeliveryHours_tbl> DeliveryHours_tbl { get; set; }
        public virtual DbSet<DeliveryType_SurgePrice> DeliveryType_SurgePrice { get; set; }
        public virtual DbSet<DeliveryType_tbl> DeliveryType_tbl { get; set; }
        public virtual DbSet<EmpBankDetails> EmpBankDetails { get; set; }
        public virtual DbSet<EmpCashReceiving_Tbl> EmpCashReceiving_Tbl { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeAddress> EmployeeAddresses { get; set; }
        public virtual DbSet<EmployeeAttendence> EmployeeAttendences { get; set; }
        public virtual DbSet<EmployeeBankAccount_Tbl> EmployeeBankAccount_Tbl { get; set; }
        public virtual DbSet<EmployeeIncentivePetrol_tbl> EmployeeIncentivePetrol_tbl { get; set; }
        public virtual DbSet<EmployeeReaction_tbl> EmployeeReaction_tbl { get; set; }
        public virtual DbSet<EmployeeRejactionReason> EmployeeRejactionReasons { get; set; }
        public virtual DbSet<EmployeeTakeOver_tbl> EmployeeTakeOver_tbl { get; set; }
        public virtual DbSet<EmpTask> EmpTasks { get; set; }
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<feedback_title> feedback_title { get; set; }
        public virtual DbSet<Footer_content> Footer_content { get; set; }
        public virtual DbSet<home_slider> home_slider { get; set; }
        public virtual DbSet<LaundryBag_Tbl> LaundryBag_Tbl { get; set; }
        public virtual DbSet<Mail_collection> Mail_collection { get; set; }
        public virtual DbSet<ManageReferral> ManageReferrals { get; set; }
        public virtual DbSet<ManageVendorService> ManageVendorServices { get; set; }
        public virtual DbSet<Offer> Offers { get; set; }
        public virtual DbSet<Order_tbl> Order_tbl { get; set; }
        public virtual DbSet<OrderCancel> OrderCancels { get; set; }
        public virtual DbSet<OrderDelivery_tbl> OrderDelivery_tbl { get; set; }
        public virtual DbSet<OrderDetail_tbl> OrderDetail_tbl { get; set; }
        public virtual DbSet<OrderProduct> OrderProducts { get; set; }
        public virtual DbSet<OrderStatus_tbl> OrderStatus_tbl { get; set; }
        public virtual DbSet<OrderType_tbl> OrderType_tbl { get; set; }
        public virtual DbSet<Otptbl> Otptbls { get; set; }
        public virtual DbSet<PriceTable> PriceTables { get; set; }
        public virtual DbSet<Promo_Type> Promo_Type { get; set; }
        public virtual DbSet<ReferalAmount_Tbl> ReferalAmount_Tbl { get; set; }
        public virtual DbSet<ReferenceType_Tbl> ReferenceType_Tbl { get; set; }
        public virtual DbSet<ReferRoleType> ReferRoleTypes { get; set; }
        public virtual DbSet<RegisteredCustomerFeedback> RegisteredCustomerFeedbacks { get; set; }
        public virtual DbSet<RoleType> RoleTypes { get; set; }
        public virtual DbSet<SalaryDetails> SalaryDetails { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<service_page_stripe> service_page_stripe { get; set; }
        public virtual DbSet<ServiceCategory> ServiceCategories { get; set; }
        public virtual DbSet<ServiceRequired_tbl> ServiceRequired_tbl { get; set; }
        public virtual DbSet<ServiceType> ServiceTypes { get; set; }
        public virtual DbSet<subscribe_content> subscribe_content { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<Surge> Surges { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskAssignment_tbl> TaskAssignment_tbl { get; set; }
        public virtual DbSet<TaskType> TaskTypes { get; set; }
        public virtual DbSet<Tax_tbl> Tax_tbl { get; set; }
        public virtual DbSet<testimonial> testimonials { get; set; }
        public virtual DbSet<TimeSlot> TimeSlots { get; set; }
        public virtual DbSet<Transaction_tbl> Transaction_tbl { get; set; }
        public virtual DbSet<Unit_Type> Unit_Type { get; set; }
        public virtual DbSet<UpperFooter> UpperFooters { get; set; }
        public virtual DbSet<Vendor> Vendors { get; set; }
        public virtual DbSet<VendorAddress> VendorAddresses { get; set; }
        public virtual DbSet<VendorBankAccount_Tbl> VendorBankAccount_Tbl { get; set; }
        public virtual DbSet<VendorPrice_tbl> VendorPrice_tbl { get; set; }
        public virtual DbSet<VendorServiceDeliveryHour_tbl> VendorServiceDeliveryHour_tbl { get; set; }
        public virtual DbSet<Welocme_area> Welocme_area { get; set; }
        public virtual DbSet<wlcm_srvic_stripe> wlcm_srvic_stripe { get; set; }
        public virtual DbSet<CancelReasonByRoleType> CancelReasonByRoleTypes { get; set; }
        public virtual DbSet<DeliveryHourstbl> DeliveryHourstbls { get; set; }
        public virtual DbSet<VendorService> VendorServices { get; set; }
        public virtual DbSet<Locality> Locality { get; set; }
        public virtual DbSet<tblImage> tblImage { get; set; }
        public virtual DbSet<CustomerFeedback> CustomerFeedbacks { get; set; }
        public virtual int GetDeliveryTypeID(string listServiceOfIDs, Nullable<int> branchid)
        {
            var listServiceOfIDsParameter = listServiceOfIDs != null ?
                new ObjectParameter("listServiceOfIDs", listServiceOfIDs) :
                new ObjectParameter("listServiceOfIDs", typeof(string));

            var branchidParameter = branchid.HasValue ?
                new ObjectParameter("Branchid", branchid) :
                new ObjectParameter("Branchid", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetDeliveryTypeID", listServiceOfIDsParameter, branchidParameter);
        }

        public virtual ObjectResult<Proc_BranchSalesTarget_Result> Proc_BranchSalesTarget()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_BranchSalesTarget_Result>("Proc_BranchSalesTarget");
        }

        public virtual ObjectResult<Proc_CashTobeSubmitted_Result> Proc_CashTobeSubmitted(Nullable<int> toEmployeeId)
        {
            var toEmployeeIdParameter = toEmployeeId.HasValue ?
                new ObjectParameter("ToEmployeeId", toEmployeeId) :
                new ObjectParameter("ToEmployeeId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_CashTobeSubmitted_Result>("Proc_CashTobeSubmitted", toEmployeeIdParameter);
        }

        public virtual int Proc_CreateBranch(string name, string address, string emailId, string faxNo, string phoneNumber, Nullable<bool> isDelete, Nullable<System.DateTime> createdDate, Nullable<int> cityId, Nullable<bool> isActive)
        {
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));

            var addressParameter = address != null ?
                new ObjectParameter("Address", address) :
                new ObjectParameter("Address", typeof(string));

            var emailIdParameter = emailId != null ?
                new ObjectParameter("EmailId", emailId) :
                new ObjectParameter("EmailId", typeof(string));

            var faxNoParameter = faxNo != null ?
                new ObjectParameter("FaxNo", faxNo) :
                new ObjectParameter("FaxNo", typeof(string));

            var phoneNumberParameter = phoneNumber != null ?
                new ObjectParameter("PhoneNumber", phoneNumber) :
                new ObjectParameter("PhoneNumber", typeof(string));

            var isDeleteParameter = isDelete.HasValue ?
                new ObjectParameter("IsDelete", isDelete) :
                new ObjectParameter("IsDelete", typeof(bool));

            var createdDateParameter = createdDate.HasValue ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(System.DateTime));

            var cityIdParameter = cityId.HasValue ?
                new ObjectParameter("CityId", cityId) :
                new ObjectParameter("CityId", typeof(int));

            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Proc_CreateBranch", nameParameter, addressParameter, emailIdParameter, faxNoParameter, phoneNumberParameter, isDeleteParameter, createdDateParameter, cityIdParameter, isActiveParameter);
        }

        public virtual ObjectResult<Proc_GetBranchInfo_Result> Proc_GetBranchInfo()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetBranchInfo_Result>("Proc_GetBranchInfo");
        }

        public virtual ObjectResult<Proc_GetCancelReasonRoleType_Result> Proc_GetCancelReasonRoleType()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetCancelReasonRoleType_Result>("Proc_GetCancelReasonRoleType");
        }

        public virtual ObjectResult<Proc_GetCustomerInProcessOrderBranchWise_Result> Proc_GetCustomerInProcessOrderBranchWise(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetCustomerInProcessOrderBranchWise_Result>("Proc_GetCustomerInProcessOrderBranchWise", branchIdParameter);
        }

        public virtual ObjectResult<Proc_GetCustomerOrderBranchWise_Result> Proc_GetCustomerOrderBranchWise(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetCustomerOrderBranchWise_Result>("Proc_GetCustomerOrderBranchWise", branchIdParameter);
        }

        public virtual ObjectResult<Proc_GetCustomerTicket_Result> Proc_GetCustomerTicket(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetCustomerTicket_Result>("Proc_GetCustomerTicket", branchIdParameter);
        }

        public virtual ObjectResult<Proc_GetEmpAddress_Result> Proc_GetEmpAddress(Nullable<int> employeeId)
        {
            var employeeIdParameter = employeeId.HasValue ?
                new ObjectParameter("EmployeeId", employeeId) :
                new ObjectParameter("EmployeeId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetEmpAddress_Result>("Proc_GetEmpAddress", employeeIdParameter);
        }

        public virtual int Proc_GetEmpAttendence(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Proc_GetEmpAttendence", branchIdParameter);
        }

        public virtual ObjectResult<Proc_GetEmployee_Result> Proc_GetEmployee()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetEmployee_Result>("Proc_GetEmployee");
        }

        public virtual ObjectResult<Proc_GetEmployeebyRoleType_Result> Proc_GetEmployeebyRoleType(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetEmployeebyRoleType_Result>("Proc_GetEmployeebyRoleType", branchIdParameter);
        }

        public virtual ObjectResult<Proc_GetEmpTicket_Result> Proc_GetEmpTicket(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetEmpTicket_Result>("Proc_GetEmpTicket", branchIdParameter);
        }

        public virtual ObjectResult<Proc_GetEmpTicketcon_Result> Proc_GetEmpTicketcon(Nullable<int> ticket)
        {
            var ticketParameter = ticket.HasValue ?
                new ObjectParameter("Ticket", ticket) :
                new ObjectParameter("Ticket", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetEmpTicketcon_Result>("Proc_GetEmpTicketcon", ticketParameter);
        }

        public virtual ObjectResult<Proc_GetGender_Result> Proc_GetGender()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetGender_Result>("Proc_GetGender");
        }

        public virtual ObjectResult<Proc_GetLeads_Result> Proc_GetLeads()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetLeads_Result>("Proc_GetLeads");
        }

        public virtual ObjectResult<Proc_GetOrders_Result> Proc_GetOrders(Nullable<int> orderId)
        {
            var orderIdParameter = orderId.HasValue ?
                new ObjectParameter("OrderId", orderId) :
                new ObjectParameter("OrderId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetOrders_Result>("Proc_GetOrders", orderIdParameter);
        }

        public virtual ObjectResult<Proc_GetPriceId_Result> Proc_GetPriceId(Nullable<int> vendorId)
        {
            var vendorIdParameter = vendorId.HasValue ?
                new ObjectParameter("VendorId", vendorId) :
                new ObjectParameter("VendorId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetPriceId_Result>("Proc_GetPriceId", vendorIdParameter);
        }


        public virtual ObjectResult<Proc_ViewVendorPrice_Result> Proc_ViewVendorPrice(Nullable<int> vendorId)
        {
            var vendorIdParameter = vendorId.HasValue ?
                new ObjectParameter("VendorId", vendorId) :
                new ObjectParameter("VendorId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_ViewVendorPrice_Result>("Proc_ViewVendorPrice", vendorIdParameter);
        }


        public virtual ObjectResult<Proc_GetPurchaseMethod_Result> Proc_GetPurchaseMethod()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetPurchaseMethod_Result>("Proc_GetPurchaseMethod");
        }

        public virtual ObjectResult<Proc_GetReportingManager_Result> Proc_GetReportingManager(Nullable<int> empId)
        {
            var empIdParameter = empId.HasValue ?
                new ObjectParameter("EmpId", empId) :
                new ObjectParameter("EmpId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetReportingManager_Result>("Proc_GetReportingManager", empIdParameter);
        }

        public virtual ObjectResult<Proc_GetVendor_Result> Proc_GetVendor()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetVendor_Result>("Proc_GetVendor");
        }

        public virtual ObjectResult<Proc_GetVendorCon_Result> Proc_GetVendorCon(Nullable<int> ticket)
        {
            var ticketParameter = ticket.HasValue ?
                new ObjectParameter("Ticket", ticket) :
                new ObjectParameter("Ticket", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Proc_GetVendorCon_Result>("Proc_GetVendorCon", ticketParameter);
        }


        public virtual int sp_Cancel_Invoice(Nullable<int> orderId)
        {
            var orderIdParameter = orderId.HasValue ?
                new ObjectParameter("OrderId", orderId) :
                new ObjectParameter("OrderId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Cancel_Invoice", orderIdParameter);
        }

        public virtual ObjectResult<SP_CollectionPending_Result> SP_CollectionPending(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_CollectionPending_Result>("SP_CollectionPending", branchIdParameter);
        }

        public virtual ObjectResult<SP_EmployeeCashPending_Result> SP_EmployeeCashPending(Nullable<int> empId)
        {
            var empIdParameter = empId.HasValue ?
                new ObjectParameter("EmpId", empId) :
                new ObjectParameter("EmpId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_EmployeeCashPending_Result>("SP_EmployeeCashPending", empIdParameter);
        }

        public virtual int SP_EmployeeNotification(Nullable<int> empId)
        {
            var empIdParameter = empId.HasValue ?
                new ObjectParameter("EmpId", empId) :
                new ObjectParameter("EmpId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_EmployeeNotification", empIdParameter);
        }

        public virtual ObjectResult<SP_EmployeeRejection_Result> SP_EmployeeRejection(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_EmployeeRejection_Result>("SP_EmployeeRejection", branchIdParameter);
        }

        public virtual ObjectResult<Nullable<double>> sp_Get_AvgPrice(Nullable<int> catId, Nullable<int> serId, Nullable<int> brnchId)
        {
            var catIdParameter = catId.HasValue ?
                new ObjectParameter("CatId", catId) :
                new ObjectParameter("CatId", typeof(int));

            var serIdParameter = serId.HasValue ?
                new ObjectParameter("SerId", serId) :
                new ObjectParameter("SerId", typeof(int));

            var brnchIdParameter = brnchId.HasValue ?
                new ObjectParameter("BrnchId", brnchId) :
                new ObjectParameter("BrnchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<double>>("sp_Get_AvgPrice", catIdParameter, serIdParameter, brnchIdParameter);
        }

        public virtual ObjectResult<SP_GetBankCashSubmisssionStatus_Result> SP_GetBankCashSubmisssionStatus(Nullable<int> manageEmpId, Nullable<System.DateTime> fROMDATE, Nullable<System.DateTime> toDate)
        {
            var manageEmpIdParameter = manageEmpId.HasValue ?
                new ObjectParameter("ManageEmpId", manageEmpId) :
                new ObjectParameter("ManageEmpId", typeof(int));

            var fROMDATEParameter = fROMDATE.HasValue ?
                new ObjectParameter("FROMDATE", fROMDATE) :
                new ObjectParameter("FROMDATE", typeof(System.DateTime));

            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("ToDate", toDate) :
                new ObjectParameter("ToDate", typeof(System.DateTime));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_GetBankCashSubmisssionStatus_Result>("SP_GetBankCashSubmisssionStatus", manageEmpIdParameter, fROMDATEParameter, toDateParameter);
        }

        public virtual int SP_GetCancelledOrder(Nullable<int> branchId, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(System.DateTime));

            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("ToDate", toDate) :
                new ObjectParameter("ToDate", typeof(System.DateTime));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetCancelledOrder", branchIdParameter, fromDateParameter, toDateParameter);
        }

        public virtual ObjectResult<Nullable<int>> SP_GetCancelOrderCount(Nullable<int> branchId, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(System.DateTime));

            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("ToDate", toDate) :
                new ObjectParameter("ToDate", typeof(System.DateTime));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SP_GetCancelOrderCount", branchIdParameter, fromDateParameter, toDateParameter);
        }

        public virtual ObjectResult<SP_GetDeliveryTime_Result> SP_GetDeliveryTime(Nullable<int> orderId)
        {
            var orderIdParameter = orderId.HasValue ?
                new ObjectParameter("OrderId", orderId) :
                new ObjectParameter("OrderId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_GetDeliveryTime_Result>("SP_GetDeliveryTime", orderIdParameter);
        }

        public virtual ObjectResult<SP_GetEmployeeAttendance_Result> SP_GetEmployeeAttendance(Nullable<int> branchid)
        {
            var branchidParameter = branchid.HasValue ?
                new ObjectParameter("Branchid", branchid) :
                new ObjectParameter("Branchid", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_GetEmployeeAttendance_Result>("SP_GetEmployeeAttendance", branchidParameter);
        }

        public virtual ObjectResult<SP_GetEmployeeEarning_Result> SP_GetEmployeeEarning(Nullable<int> employeeId)
        {
            var employeeIdParameter = employeeId.HasValue ?
                new ObjectParameter("EmployeeId", employeeId) :
                new ObjectParameter("EmployeeId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_GetEmployeeEarning_Result>("SP_GetEmployeeEarning", employeeIdParameter);
        }


        public virtual int SP_GETMYTASK(Nullable<int> eMPIID)
        {
            var eMPIIDParameter = eMPIID.HasValue ?
                new ObjectParameter("EMPIID", eMPIID) :
                new ObjectParameter("EMPIID", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GETMYTASK", eMPIIDParameter);
        }



        public virtual int SP_GetPendingPickUps(Nullable<int> branchid)
        {
            var branchidParameter = branchid.HasValue ?
                new ObjectParameter("Branchid", branchid) :
                new ObjectParameter("Branchid", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetPendingPickUps", branchidParameter);
        }



        public virtual int SP_GetPreapredOrdersForAssignment(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetPreapredOrdersForAssignment", branchIdParameter);
        }



        public virtual int SP_GetVendorHandoverTask(Nullable<int> empId)
        {
            var empIdParameter = empId.HasValue ?
                new ObjectParameter("EmpId", empId) :
                new ObjectParameter("EmpId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetVendorHandoverTask", empIdParameter);
        }

        public virtual int SP_GetVendorListForAssignment(Nullable<int> orderid)
        {
            var orderidParameter = orderid.HasValue ?
                new ObjectParameter("Orderid", orderid) :
                new ObjectParameter("Orderid", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetVendorListForAssignment", orderidParameter);
        }

        public virtual ObjectResult<Nullable<int>> SP_GetVendorMaxDeliveryHours(Nullable<int> pickUpId)
        {
            var pickUpIdParameter = pickUpId.HasValue ?
                new ObjectParameter("PickUpId", pickUpId) :
                new ObjectParameter("PickUpId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SP_GetVendorMaxDeliveryHours", pickUpIdParameter);
        }

        public virtual int SP_GetVendorPreparedTask(Nullable<int> vendorId)
        {
            var vendorIdParameter = vendorId.HasValue ?
                new ObjectParameter("VendorId", vendorId) :
                new ObjectParameter("VendorId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetVendorPreparedTask", vendorIdParameter);
        }

        public virtual int SP_GetVendorSurgePercentage(Nullable<int> taskId)
        {
            var taskIdParameter = taskId.HasValue ?
                new ObjectParameter("TaskId", taskId) :
                new ObjectParameter("TaskId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetVendorSurgePercentage", taskIdParameter);
        }

        public virtual int sp_Order_status(Nullable<int> id, Nullable<int> orderStatusId)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            var orderStatusIdParameter = orderStatusId.HasValue ?
                new ObjectParameter("OrderStatusId", orderStatusId) :
                new ObjectParameter("OrderStatusId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Order_status", idParameter, orderStatusIdParameter);
        }





        public virtual ObjectResult<Nullable<double>> sp_SumOrderdtls(Nullable<int> orderId)
        {
            var orderIdParameter = orderId.HasValue ?
                new ObjectParameter("OrderId", orderId) :
                new ObjectParameter("OrderId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<double>>("sp_SumOrderdtls", orderIdParameter);
        }

        public virtual ObjectResult<Nullable<double>> SP_TotalPendingAmount(Nullable<int> branchId)
        {
            var branchIdParameter = branchId.HasValue ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<double>>("SP_TotalPendingAmount", branchIdParameter);
        }

        public virtual int sp_Update_CustomerDts(string emailId, Nullable<long> mob_no, Nullable<int> id)
        {
            var emailIdParameter = emailId != null ?
                new ObjectParameter("EmailId", emailId) :
                new ObjectParameter("EmailId", typeof(string));

            var mob_noParameter = mob_no.HasValue ?
                new ObjectParameter("Mob_no", mob_no) :
                new ObjectParameter("Mob_no", typeof(long));

            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_CustomerDts", emailIdParameter, mob_noParameter, idParameter);
        }

        public virtual int sp_Update_CustomerPwd(string pwd, Nullable<int> id)
        {
            var pwdParameter = pwd != null ?
                new ObjectParameter("Pwd", pwd) :
                new ObjectParameter("Pwd", typeof(string));

            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_CustomerPwd", pwdParameter, idParameter);
        }

        public virtual int sp_Update_Make_My_Package(Nullable<int> customerId, Nullable<int> balance)
        {
            var customerIdParameter = customerId.HasValue ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(int));

            var balanceParameter = balance.HasValue ?
                new ObjectParameter("Balance", balance) :
                new ObjectParameter("Balance", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_Make_My_Package", customerIdParameter, balanceParameter);
        }

        public virtual int sp_Update_MyPackage(Nullable<int> id, Nullable<int> balance)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            var balanceParameter = balance.HasValue ?
                new ObjectParameter("Balance", balance) :
                new ObjectParameter("Balance", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_MyPackage", idParameter, balanceParameter);
        }

        public virtual int sp_Update_MyreferCode(string myReferalCode, string name, Nullable<int> id)
        {
            var myReferalCodeParameter = myReferalCode != null ?
                new ObjectParameter("MyReferalCode", myReferalCode) :
                new ObjectParameter("MyReferalCode", typeof(string));

            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));

            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_MyreferCode", myReferalCodeParameter, nameParameter, idParameter);
        }

        public virtual int sp_Update_Order_tbl(Nullable<int> id, Nullable<int> paymentTypeId, Nullable<int> orderStatusId)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            var paymentTypeIdParameter = paymentTypeId.HasValue ?
                new ObjectParameter("PaymentTypeId", paymentTypeId) :
                new ObjectParameter("PaymentTypeId", typeof(int));

            var orderStatusIdParameter = orderStatusId.HasValue ?
                new ObjectParameter("OrderStatusId", orderStatusId) :
                new ObjectParameter("OrderStatusId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_Order_tbl", idParameter, paymentTypeIdParameter, orderStatusIdParameter);
        }

        public virtual int sp_Update_promocode(Nullable<int> usedCount, Nullable<int> id)
        {
            var usedCountParameter = usedCount.HasValue ?
                new ObjectParameter("UsedCount", usedCount) :
                new ObjectParameter("UsedCount", typeof(int));

            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_promocode", usedCountParameter, idParameter);
        }

        public virtual int sp_Update_Wallet(Nullable<int> id, Nullable<int> net_Amount, Nullable<System.DateTime> modify_Date)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            var net_AmountParameter = net_Amount.HasValue ?
                new ObjectParameter("Net_Amount", net_Amount) :
                new ObjectParameter("Net_Amount", typeof(int));

            var modify_DateParameter = modify_Date.HasValue ?
                new ObjectParameter("Modify_Date", modify_Date) :
                new ObjectParameter("Modify_Date", typeof(System.DateTime));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Update_Wallet", idParameter, net_AmountParameter, modify_DateParameter);
        }

        public virtual int sp_UpdateOrderDtls(Nullable<int> id, Nullable<int> qunatity)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));

            var qunatityParameter = qunatity.HasValue ?
                new ObjectParameter("Qunatity", qunatity) :
                new ObjectParameter("Qunatity", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_UpdateOrderDtls", idParameter, qunatityParameter);
        }



        public virtual int SP_VendorGetHandoverTask(Nullable<int> vendorId)
        {
            var vendorIdParameter = vendorId.HasValue ?
                new ObjectParameter("VendorId", vendorId) :
                new ObjectParameter("VendorId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_VendorGetHandoverTask", vendorIdParameter);
        }

        public virtual int SP_VendorGetMyTask(Nullable<int> vendorId)
        {
            var vendorIdParameter = vendorId.HasValue ?
                new ObjectParameter("VendorId", vendorId) :
                new ObjectParameter("VendorId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_VendorGetMyTask", vendorIdParameter);
        }

        public virtual ObjectResult<SP_ViewPromoCode_Result> SP_ViewPromoCode()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_ViewPromoCode_Result>("SP_ViewPromoCode");
        }
    }
}